
@interface Box2DHelper : NSObject

+ (float) pointsPerMeter;
+ (float) metersPerPoint;

+ (float) pixelsPerMeter;
+ (float) metersPerPixel;

@end
