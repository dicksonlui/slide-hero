
#import "Game.h"
#import "Hero.h"
#import "HeroContactListener.h"
#import "Box2D.h"
#import "Constants.h"
#import "Box2DHelper.h"

@interface Hero()
- (void) createBox2DBody;
@end

@implementation Hero

@synthesize game = _game;
@synthesize sprite = _sprite;
@synthesize awake = _awake;
@synthesize diving = _diving;

+ (id) heroWithGame:(Game*)game {
	return [[[self alloc] initWithGame:game] autorelease];
}

- (id) initWithGame:(Game*)game {
    // initialize hero with image, radius, and physics listener
	if ((self = [super init])) {
		self.game = game;
#ifndef DRAW_BOX2D_WORLD
		self.sprite = [CCSprite spriteWithFile:@"hero.png"];
		[self addChild:_sprite];
#endif
		_body = NULL;
		_radius = 14.0f;
		_contactListener = new HeroContactListener(self);
		_game.world->SetContactListener(_contactListener);
		[self reset];
	}
	return self;
}

- (void) dealloc {
    // memory deallocation
	self.game = nil;
#ifndef DRAW_BOX2D_WORLD
	self.sprite = nil;
#endif
	delete _contactListener;
	[super dealloc];
}

- (void) reset {
    // Reset the hero to no streak, not flying or diving and to reset position
	_flying = NO;
	_diving = NO;
	_nPerfectSlides = 0;
	if (_body) {
		_game.world->DestroyBody(_body);
	}
	[self createBox2DBody];
	[self updateNode];
	[self sleep];
}

- (void) createBox2DBody {
    // Box2D Physical specs of hero object
	b2BodyDef bd;
	bd.type = b2_dynamicBody;
	bd.linearDamping = 0.05f;
	bd.fixedRotation = true;
    
    // start position
	CGPoint p = ccp(0, _game.screenH/2+_radius);
	bd.position.Set(p.x * [Box2DHelper metersPerPoint], p.y * [Box2DHelper metersPerPoint]);
	_body = _game.world->CreateBody(&bd);
	b2CircleShape shape;
	shape.m_radius = _radius * [Box2DHelper metersPerPoint];
	b2FixtureDef fd;
    
    // properties
	fd.shape = &shape;
	fd.density = 1.0f;
	fd.restitution = 0; // bounce
	fd.friction = 0;
	_body->CreateFixture(&fd);
}

- (void) sleep {
    // sleep
	_awake = NO;
	_body->SetActive(false);
}

- (void) wake {
    // wake
	_awake = YES;
	_body->SetActive(true);
	_body->ApplyLinearImpulse(b2Vec2(1,2), _body->GetPosition());
}

- (void) updatePhysics {
    // moving body
    
    // if diving, then apply a downward force onto the box2d body
	if (_diving) {
		if (!_awake) {
			[self wake];
			_diving = NO;
		} else {
			_body->ApplyForce(b2Vec2(0,-40),_body->GetPosition());
		}
	}
    
    // set minimum velocities so game keeps moving properly
	const float minVelocityX = 3;
	const float minVelocityY = -40;
	b2Vec2 vel = _body->GetLinearVelocity();
    
    // make sure velocities do not go under minimum point
	if (vel.x < minVelocityX) {
		vel.x = minVelocityX;
	}
	if (vel.y < minVelocityY) {
		vel.y = minVelocityY;
	}
    
    // return new updated velocity
	_body->SetLinearVelocity(vel);
}

- (void) updateNode {
	
    // update position  and velocityof hero
	CGPoint p;
	p.x = _body->GetPosition().x * [Box2DHelper pointsPerMeter];
	p.y = _body->GetPosition().y * [Box2DHelper pointsPerMeter];
	self.position = p;
	b2Vec2 vel = _body->GetLinearVelocity();
	float angle = atan2f(vel.y, vel.x);
#ifdef DRAW_BOX2D_WORLD
	_body->SetTransform(_body->GetPosition(), angle);
#else
	self.rotation = -1 * CC_RADIANS_TO_DEGREES(angle);
#endif
	b2Contact *c = _game.world->GetContactList();
    
    // Change properties of hero depending on what has happened on the last slide
	if (c) {
		if (_flying) {
			[self landed];
		}
	} else {
		if (!_flying) {
			[self tookOff];
		}
	}
	if (p.y < -_radius && _awake) {
		[self sleep];
	}
}

- (void) landed {
    // hero no longer flying
	_flying = NO;
}

- (void) tookOff {
    
    // taken off, if velocity is above a certain point then show a perfect slide
	_flying = YES;
	b2Vec2 vel = _body->GetLinearVelocity();
	if (vel.y > kPerfectTakeOffVelocityY) {
		_nPerfectSlides++;
		if (_nPerfectSlides > 1) {
            // If you have a streak of perfect slides, then give a perfect streak
			if (_nPerfectSlides == 4) {
				[_game showFrenzy];
            // otherwise, just show a perfect slide and continue on
			} else {
				[_game showPerfectSlide];
			}
		}
	}
}

- (void) hit {
    
    // BAD SLIDE, reset perfect slides and show hit label
	_nPerfectSlides = 0;
	[_game showHit];
}

- (void) setDiving:(BOOL)diving {
    // if not already diving, set to diving
    
	if (_diving != diving) {
		_diving = diving;
	}
}

@end
