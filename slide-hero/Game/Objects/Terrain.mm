
#import "Terrain.h"
#import "Constants.h"
#import "Box2DHelper.h"

@interface Terrain()
- (CCSprite*) generateStripesSprite;
- (CCTexture2D*) generateStripesTexture;
- (void) renderStripes;
- (void) renderGradient;
- (void) renderTopBorder;
- (void) generateHillKeyPoints;
- (void) generateBorderVertices;
- (void) createBox2DBody;
- (void) resetHillVertices;
- (ccColor4F) randomColor;
@end

@implementation Terrain

// Stripe textures for hill
@synthesize stripes = _stripes;
// Distance between hills: play with this!
@synthesize offsetX = _offsetX;

+ (id) terrainWithWorld:(b2World*)w {
	return [[[self alloc] initWithWorld:w] autorelease];
}

- (id) initWithWorld:(b2World*)w {
	
	if ((self = [super init])) {
		
        // initialize the world
		world = w;
		CGSize size = [[CCDirector sharedDirector] winSize];
		screenW = size.width;
		screenH = size.height;
		
#ifndef DRAW_BOX2D_WORLD
		textureSize = 1024;
		self.stripes = [self generateStripesSprite];
#endif
		
        // set up hill vertices
		[self generateHillKeyPoints];
		[self generateBorderVertices];
		[self createBox2DBody];

		self.offsetX = 0;
	}
	return self;
}

- (void) dealloc {

// overwrite default dealloc method because we must dealloc user objs
    
#ifndef DRAW_BOX2D_WORLD
	self.stripes = nil;
#endif
	[super dealloc];
}

- (CCSprite*) generateStripesSprite {
	
	CCTexture2D *texture = [self generateStripesTexture];
	CCSprite *sprite = [CCSprite spriteWithTexture:texture];
	ccTexParams tp = {GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_CLAMP_TO_EDGE};
	[sprite.texture setTexParameters:&tp];
	
	return sprite;
}

- (CCTexture2D*) generateStripesTexture {
    
	// This method renders the texture and turns off drawing into the texture
	CCRenderTexture *rt = [CCRenderTexture renderTextureWithWidth:textureSize height:textureSize];
    
    // This sets up OpenGL so that any further drawing draws into CCRenderTexture
	[rt begin];
	[self renderStripes];
	[self renderGradient];
	[self renderTopBorder];
	[rt end];
	
	return rt.sprite.texture;
}


- (void) draw {
	// Draw the world using raw OpenGL commands
#ifdef DRAW_BOX2D_WORLD
	glDisable(GL_TEXTURE_2D);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
	glPushMatrix();
	glScalef(CC_CONTENT_SCALE_FACTOR(), CC_CONTENT_SCALE_FACTOR(), 1.0f);
	world->DrawDebugData();
	glPopMatrix();
	glEnableClientState(GL_COLOR_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnable(GL_TEXTURE_2D);
	
#else
	glBindTexture(GL_TEXTURE_2D, _stripes.texture.name);
	glDisableClientState(GL_COLOR_ARRAY);
	glColor4f(1, 1, 1, 1);
	glVertexPointer(2, GL_FLOAT, 0, hillVertices);
	glTexCoordPointer(2, GL_FLOAT, 0, hillTexCoords);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei)nHillVertices);
	glEnableClientState(GL_COLOR_ARRAY);
#endif
}

- (void) setOffsetX:(float)offsetX {
    // This method is used to set the offset between hill vertices
	static BOOL firstTime = YES;
	if (_offsetX != offsetX || firstTime) {
		firstTime = NO;
		_offsetX = offsetX;
		self.position = ccp(screenW/8-_offsetX*self.scale, 0);
		[self resetHillVertices];
	}
}

- (void) renderStripes {
	
    // Generate a random amount of stripes to create some variance in texture
	const int minStripes = 4;
	const int maxStripes = 30;
	int nStripes = arc4random_uniform(maxStripes-minStripes)+minStripes;
	if (nStripes%2) {
		nStripes++;
	}
    
    // Draw the stripes
	ccVertex2F *vertices = (ccVertex2F*)malloc(sizeof(ccVertex2F)*nStripes*6);
	ccColor4F *colors = (ccColor4F*)malloc(sizeof(ccColor4F)*nStripes*6);
	int nVertices = 0;
	float x1, x2, y1, y2, dx, dy;
	ccColor4F c;
	if (arc4random_uniform(2)) {
		dx = (float)textureSize*2 / (float)nStripes;
		dy = 0;
		x1 = -textureSize;
		y1 = 0;
		x2 = 0;
		y2 = textureSize;
		for (int i=0; i<nStripes/2; i++) {
			c = [self randomColor];
			for (int j=0; j<2; j++) {
				for (int k=0; k<6; k++) {
					colors[nVertices+k] = c;
				}
                // set the 6 stripes as different vertices
				vertices[nVertices++] = (ccVertex2F){x1+j*textureSize, y1};
				vertices[nVertices++] = (ccVertex2F){x1+j*textureSize+dx, y1};
				vertices[nVertices++] = (ccVertex2F){x2+j*textureSize, y2};
				vertices[nVertices++] = vertices[nVertices-3];
				vertices[nVertices++] = vertices[nVertices-3];
				vertices[nVertices++] = (ccVertex2F){x2+j*textureSize+dx, y2};
			}
			x1 += dx;
			x2 += dx;
		}
	} else {
		dx = 0;
		dy = (float)textureSize / (float)nStripes;
		x1 = 0;
		y1 = 0;
		x2 = textureSize;
		y2 = 0;
		for (int i=0; i<nStripes; i++) {
			c = [self randomColor];
			for (int k=0; k<6; k++) {
				colors[nVertices+k] = c;
			}
            // set the 6 stripes as different vertices
			vertices[nVertices++] = (ccVertex2F){x1, y1};
			vertices[nVertices++] = (ccVertex2F){x2, y2};
			vertices[nVertices++] = (ccVertex2F){x1, y1+dy};
			vertices[nVertices++] = vertices[nVertices-3];
			vertices[nVertices++] = vertices[nVertices-3];
			vertices[nVertices++] = (ccVertex2F){x2, y2+dy};
			y1 += dy;
			y2 += dy;
		}
		
	}
    
    // scale
	for (int i=0; i<nVertices; i++) {
		vertices[i].x *= CC_CONTENT_SCALE_FACTOR();
		vertices[i].y *= CC_CONTENT_SCALE_FACTOR();
	}
    
    // Call OpenGL commands to draw the textures
	glDisable(GL_TEXTURE_2D);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glColor4f(1, 1, 1, 1);
	glVertexPointer(2, GL_FLOAT, 0, vertices);
	glColorPointer(4, GL_FLOAT, 0, colors);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
	glDrawArrays(GL_TRIANGLES, 0, (GLsizei)nVertices);
    
    // Memory alloc
	free(vertices);
	free(colors);
}

- (void) renderGradient {
	
    // Draw the gradient into the texture
	float gradientAlpha = 0.5f;
	float gradientWidth = textureSize;
	ccVertex2F vertices[6];
	ccColor4F colors[6];
	int nVertices = 0;
    
    // Create two triangles (diagonal through the screen) to create our gradient
	vertices[nVertices] = (ccVertex2F){0, 0};
	colors[nVertices++] = (ccColor4F){0, 0, 0, 0};
	vertices[nVertices] = (ccVertex2F){textureSize, 0};
	colors[nVertices++] = (ccColor4F){0, 0, 0, 0};
	vertices[nVertices] = (ccVertex2F){0, gradientWidth};
	colors[nVertices++] = (ccColor4F){0, 0, 0, gradientAlpha};
	vertices[nVertices] = (ccVertex2F){textureSize, gradientWidth};
	colors[nVertices++] = (ccColor4F){0, 0, 0, gradientAlpha};
    
    // handling edgier cases in which width < size and wont fit
	if (gradientWidth < textureSize) {
		vertices[nVertices] = (ccVertex2F){0, textureSize};
		colors[nVertices++] = (ccColor4F){0, 0, 0, gradientAlpha};
		vertices[nVertices] = (ccVertex2F){textureSize, textureSize};
		colors[nVertices++] = (ccColor4F){0, 0, 0, gradientAlpha};
	}
    
    // scale
	for (int i=0; i<nVertices; i++) {
		vertices[i].x *= CC_CONTENT_SCALE_FACTOR();
		vertices[i].y *= CC_CONTENT_SCALE_FACTOR();
	}
    
    // OpenGL raw commands to draw stripes from processed vertices
	glVertexPointer(2, GL_FLOAT, 0, vertices);
	glColorPointer(4, GL_FLOAT, 0, colors);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei)nVertices);
}

- (void) renderTopBorder {
    
    // set border properties/width
	float borderAlpha = 0.5f;
	float borderWidth = 2.0f;
    
    // 2 vertices to anchor the border
	ccVertex2F vertices[2];
	int nVertices = 0;
	vertices[nVertices++] = (ccVertex2F){0, borderWidth/2};
	vertices[nVertices++] = (ccVertex2F){textureSize, borderWidth/2};
    
    // scale
	for (int i=0; i<nVertices; i++) {
		vertices[i].x *= CC_CONTENT_SCALE_FACTOR();
		vertices[i].y *= CC_CONTENT_SCALE_FACTOR();
	}
    
    // draw border along hill vertices
	glDisableClientState(GL_COLOR_ARRAY);
	glLineWidth(borderWidth*CC_CONTENT_SCALE_FACTOR());
	glColor4f(0, 0, 0, borderAlpha);
	glVertexPointer(2, GL_FLOAT, 0, vertices);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDrawArrays(GL_LINE_STRIP, 0, (GLsizei)nVertices);
}

- (void) generateHillKeyPoints {
    
	nHillKeyPoints = 0;
	float x, y, dx, dy, ny;
    // Set x to a fourth of the screen width
	x = -screenW/4;
    // Set y to three/fourths of the screen height
	y = screenH*3/4;
    // Create the vertex
	hillKeyPoints[nHillKeyPoints++] = (ccVertex2F){x, y};
    
    // Repeat with different values
	x = 0;
	y = screenH/2;
	hillKeyPoints[nHillKeyPoints++] = (ccVertex2F){x, y};
    
    // Increment x-axis in the range of 160 + a random number between 0 and 80 for more variation
	int minDX = 160, rangeDX = 80;
    // Increment y-axis in the range of 60 + a random number between 0 and 60 for variation in hills.
	int minDY = 60,  rangeDY = 60;
	float sign = -1;
    
    // Don't allow hills to be drawn above or below the screen
	float maxHeight = screenH;
	float minHeight = 20;
	while (nHillKeyPoints < kMaxHillKeyPoints-1) {
		dx = arc4random_uniform(rangeDX)+minDX;
		x += dx;
		dy = arc4random_uniform(rangeDY)+minDY;
		ny = y + dy*sign;
		if(ny > maxHeight) ny = maxHeight;
		if(ny < minHeight) ny = minHeight;
		y = ny;
		sign *= -1;
		hillKeyPoints[nHillKeyPoints++] = (ccVertex2F){x, y};
	}
	x += minDX+rangeDX;
	y = 0;
	hillKeyPoints[nHillKeyPoints++] = (ccVertex2F){x, y};
	for (int i=0; i<nHillKeyPoints; i++) {
		hillKeyPoints[i].x *= CC_CONTENT_SCALE_FACTOR();
		hillKeyPoints[i].y *= CC_CONTENT_SCALE_FACTOR();
	}
	fromKeyPointI = 0;
	toKeyPointI = 0;
}

- (void) generateBorderVertices {
    
	nBorderVertices = 0;
    // create 4 cc vertex
	ccVertex2F p0, p1, pt0, pt1;
	p0 = hillKeyPoints[0];
    
    // for each hill key point, set the border vertex
	for (int i=1; i<nHillKeyPoints; i++) {
		p1 = hillKeyPoints[i];
		int hSegments = floorf((p1.x-p0.x)/kHillSegmentWidth);
		float dx = (p1.x - p0.x) / hSegments;
		float da = M_PI / hSegments;
		float ymid = (p0.y + p1.y) / 2;
		float ampl = (p0.y - p1.y) / 2;
		pt0 = p0;
		borderVertices[nBorderVertices++] = pt0;
		for (int j=1; j<hSegments+1; j++) {
			pt1.x = p0.x + j*dx;
			pt1.y = ymid + ampl * cosf(da*j);
			borderVertices[nBorderVertices++] = pt1;
			pt0 = pt1;
		}
		p0 = p1;
	}
}

- (void) createBox2DBody {
    // creates the box2d body for collisions with the hero
	b2BodyDef bd;
	bd.position.Set(0, 0);
	body = world->CreateBody(&bd);
    
    // create an array with number of vertices
	b2Vec2 b2vertices[kMaxBorderVertices];
	int nVertices = 0;
    
    // use box2d methods to set collison boundaries
	for (int i=0; i<nBorderVertices; i++) {
		b2vertices[nVertices++].Set(borderVertices[i].x * [Box2DHelper metersPerPixel],borderVertices[i].y * [Box2DHelper metersPerPixel]);
	}
	b2vertices[nVertices++].Set(borderVertices[nBorderVertices-1].x * [Box2DHelper metersPerPixel], 0);
	b2vertices[nVertices++].Set(borderVertices[0].x * [Box2DHelper metersPerPixel], 0);
    
    // create body
	b2LoopShape shape;
	shape.Create(b2vertices, nVertices);
	body->CreateFixture(&shape, 0);
}

- (void) resetHillVertices {
#ifdef DRAW_BOX2D_WORLD
	return;
#endif
	static int prevFromKeyPointI = -1;
	static int prevToKeyPointI = -1;
    
    // scale
	float leftSideX = _offsetX-screenW/8/self.scale;
	float rightSideX = _offsetX+screenW*7/8/self.scale;
	leftSideX *= CC_CONTENT_SCALE_FACTOR();
	rightSideX *= CC_CONTENT_SCALE_FACTOR();
    
    // handle scrolling with hill key points
	while (hillKeyPoints[fromKeyPointI+1].x < leftSideX) {
		fromKeyPointI++;
		if (fromKeyPointI > nHillKeyPoints-1) {
			fromKeyPointI = nHillKeyPoints-1;
			break;
		}
	}
	while (hillKeyPoints[toKeyPointI].x < rightSideX) {
		toKeyPointI++;
		if (toKeyPointI > nHillKeyPoints-1) {
			toKeyPointI = nHillKeyPoints-1;
			break;
		}
	}
    
    // Draw the hill curve using cosine curve
	if (prevFromKeyPointI != fromKeyPointI || prevToKeyPointI != toKeyPointI) {
		nHillVertices = 0;
		ccVertex2F p0, p1, pt0, pt1;
        
        // Fill up an array wit hthe vertices for each segment of the hill
		p0 = hillKeyPoints[fromKeyPointI];
		for (int i=fromKeyPointI+1; i<toKeyPointI+1; i++) {
			p1 = hillKeyPoints[i];
			int hSegments = floorf((p1.x-p0.x)/kHillSegmentWidth);
			int vSegments = 1;
			float dx = (p1.x-p0.x)/hSegments;
			float da = M_PI / hSegments;
			float ymid = (p0.y + p1.y) / 2;
			float ampl = (p0.y - p1.y) / 2;
			pt0 = p0;
            // Each segment requries four vertices and four texture coordinates
			for (int j=1; j<hSegments+1; j++) {
				pt1.x = p0.x + j*dx;
				pt1.y = ymid + ampl * cosf(da*j);
				for (int k=0; k<vSegments+1; k++) {
					hillVertices[nHillVertices] = (ccVertex2F){pt0.x, pt0.y-(float)textureSize/vSegments*k};
					hillTexCoords[nHillVertices++] = (ccVertex2F){pt0.x/(float)textureSize, (float)(k)/vSegments};
					hillVertices[nHillVertices] = (ccVertex2F){pt1.x, pt1.y-(float)textureSize/vSegments*k};
					hillTexCoords[nHillVertices++] = (ccVertex2F){pt1.x/(float)textureSize, (float)(k)/vSegments};
				}
				pt0 = pt1;
			}
			p0 = p1;
		}
		prevFromKeyPointI = fromKeyPointI;
		prevToKeyPointI = toKeyPointI;
	}
}

// return a color
- (ccColor4F) randomColor {
    int r, g, b;
    r = 0;
    g = 255;
    b = 0;
	return ccc4FFromccc3B(ccc3(r, g, b));
}


// reset terrain
- (void) reset {
#ifndef DRAW_BOX2D_WORLD
	self.stripes = [self generateStripesSprite];
#endif
	fromKeyPointI = 0;
	toKeyPointI = 0;
}

@end
