This is the culmination of all my efforts to make a physics-based iOS game. This game features:

* Randomly generated hill curves and visuals to increase replay value
* Box2D physics engine to simulate sliding and flying
* Skill based point scoring system

Tools used:

* Cocos2D
* Box2D
* Objective-C