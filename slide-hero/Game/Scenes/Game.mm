
#import "StartMenuLayer.h"
#import "Game.h"
#import "Sky.h"
#import "Terrain.h"
#import "Hero.h"
#import "Constants.h"
#import "Box2DHelper.h"

@interface Game()
- (void) createBox2DWorld;
- (BOOL) touchBeganAt:(CGPoint)location;
- (BOOL) touchEndedAt:(CGPoint)location;
- (void) reset;
@end

@implementation Game

@synthesize screenW = _screenW;
@synthesize screenH = _screenH;
@synthesize world = _world;
@synthesize sky = _sky;
@synthesize terrain = _terrain;
@synthesize hero = _hero;
@synthesize resetButton = _resetButton;
@synthesize deadButton = _deadButton;

+ (CCScene*) scene {
    // Create and return the scene
	CCScene *scene = [CCScene node];
	[scene addChild:[Game node]];
	return scene;
}

- (id) init {
	if ((self = [super init])) {
        
        // Initialize the world
		CGSize screenSize = [[CCDirector sharedDirector] winSize];
		_screenW = screenSize.width;
		_screenH = screenSize.height;
		[self createBox2DWorld];
        
#ifndef DRAW_BOX2D_WORLD
        // Initialize the sky textures
		self.sky = [Sky skyWithTextureSize:1024];
		[self addChild:_sky];
#endif
        // Initialize the ground terrain
		self.terrain = [Terrain terrainWithWorld:_world];
		[self addChild:_terrain];
        
        // Initialize the player
		self.hero = [Hero heroWithGame:self];
		[_terrain addChild:_hero];
        dead = false;
        
        // Create the reset button on the top right of the screen
		self.resetButton = [CCSprite spriteWithFile:@"resetButton.png"];
		[self addChild:_resetButton];
		CGSize size = _resetButton.contentSize;
		float padding = 8;
		_resetButton.position = ccp(_screenW-size.width/2-padding, _screenH-size.height/2-padding);
        
        // Create score label
        int currScore = 0;
        _scoreLabel = [CCLabelTTF labelWithString:@"0" dimensions:CGSizeMake(200,30) alignment:UITextAlignmentRight fontName:@"Marker Felt" fontSize:30];
        [self addChild:_scoreLabel];
        _scoreLabel.position = ccp(0, screenSize.height-20);
        
#ifdef __IPHONE_OS_VERSION_MAX_ALLOWED
		self.isTouchEnabled = YES;
#else
		self.isMouseEnabled = YES;
#endif
		[self scheduleUpdate];
	}
	return self;
}

#pragma mark touches

#ifdef __IPHONE_OS_VERSION_MAX_ALLOWED

- (void) registerWithTouchDispatcher {
	[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

- (BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
	CGPoint location = [touch locationInView:[touch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];
	return [self touchBeganAt:location];;
}

- (void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
	CGPoint location = [touch locationInView:[touch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];
	[self touchEndedAt:location];;
}

#else

- (void) registerWithTouchDispatcher {
	[[CCEventDispatcher sharedDispatcher] addMouseDelegate:self priority:0];
}

- (BOOL)ccMouseDown:(NSEvent *)event {
	CGPoint location = [[CCDirector sharedDirector] convertEventToGL:event];
	return [self touchBeganAt:location];
}

- (BOOL)ccMouseUp:(NSEvent *)event {
	CGPoint location = [[CCDirector sharedDirector] convertEventToGL:event];
	return [self touchEndedAt:location];
}

#endif

- (BOOL) touchBeganAt:(CGPoint)location {
    // if touching reset button, reset game
	CGPoint pos = _resetButton.position;
	CGSize size = _resetButton.contentSize;
	float padding = 8;
	float w = size.width+padding*2;
	float h = size.height+padding*2;
	CGRect rect = CGRectMake(pos.x-w/2, pos.y-h/2, w, h);
    
    // reset or diving
	if (CGRectContainsPoint(rect, location)) {
		[self reset];
	} else {
		_hero.diving = YES;
	}
	return YES;
}

- (BOOL) touchEndedAt:(CGPoint)location {
    // Touch ended, hero is no longer diving
	_hero.diving = NO;
	return YES;
}

-(void) updatePerfectScore {
    // +1 to score
    if (dead == false) {
        currScore = currScore + 1;
    }
    [_scoreLabel setString: [NSString stringWithFormat:@"%d", currScore]];
}

-(void) updateSpreeScore {
    // +4 to score
    if (dead == false) {
        currScore = currScore + 4;
    }
    [_scoreLabel setString: [NSString stringWithFormat:@"%d", currScore]];
}

-(void) updateBadScore {
    // -1 to score, can kill player
    if (dead == false) {
        currScore = currScore - 1;
        if (currScore < 0) {
            dead = true;
            [self deadPlayer];
        }
    }
    [_scoreLabel setString: [NSString stringWithFormat:@"%d", currScore]];
}

-(void) deadPlayer {
    
    // When score drops below 0, player loses and can no longer play
    
    self.deadButton = [CCSprite spriteWithFile:@"loserscreen.png"];
    _deadButton.position = ccp(240,160);
    [self addChild:_deadButton];
}

#pragma mark methods

- (void) reset {
    [_terrain reset];
    [_hero reset];
    [self removeChild:_deadButton cleanup:YES];
    currScore = 0;
    [_scoreLabel setString: [NSString stringWithFormat:@"%d", currScore]];
    dead = false;
}

- (void) update:(ccTime)dt {

    // Update player physics
	[_hero updatePhysics];
	int32 velocityIterations = 8;
	int32 positionIterations = 3;
	_world->Step(dt, velocityIterations, positionIterations);
	[_hero updateNode];
    
    // Do not allow the player to go under the terrain
	float height = _hero.position.y;
	const float minHeight = _screenH*4/5;
	if (height < minHeight) {
		height = minHeight;
	}
	float scale = minHeight / height;
	_terrain.scale = scale;
	_terrain.offsetX = _hero.position.x;
    
#ifndef DRAW_BOX2D_WORLD
	[_sky setOffsetX:_terrain.offsetX*0.2f];
	[_sky setScale:1.0f-(1.0f-scale)*0.75f];
#endif
}

- (void) createBox2DWorld {
    
	// Creates Box2D Physics World
	b2Vec2 gravity;
	gravity.Set(0.0f, -9.8f);
	_world = new b2World(gravity, false);

#ifdef DRAW_BOX2D_WORLD
	_render = new GLESDebugDraw([Box2DHelper pointsPerMeter]);
	_world->SetDebugDraw(_render);
	uint32 flags = 0;
	flags += b2Draw::e_shapeBit;
	_render->SetFlags(flags);
#endif
}

- (void) showPerfectSlide {
    // A perfect slide, netting a point
    
    // Display PERFECT on the bottom of the screen to let users know what they accomplished
	NSString *str = @"PERFECT";
	CCLabelBMFont *label = [CCLabelBMFont labelWithString:str fntFile:@"good_dog_plain_32.fnt"];
	label.position = ccp(_screenW/2, _screenH/16);
	[label runAction:[CCScaleTo actionWithDuration:1.0f scale:1.2f]];
    
    // Update Score Method
    [self updatePerfectScore];
    
    // Remove label
	[label runAction:[CCSequence actions: [CCFadeOut actionWithDuration:1.0f],[CCCallFuncND actionWithTarget:label selector:@selector(removeFromParentAndCleanup:) data:(void*)YES],
					  nil]];
	[self addChild:label];
}

- (void) showFrenzy {
    // A perfect slide STREAK, netting 4 points
    
    // Display PERFECT STREAK on the bottom of the screen to let users know what they accomplished
	NSString *str = @"PERFECT STREAK";
	CCLabelBMFont *label = [CCLabelBMFont labelWithString:str fntFile:@"good_dog_plain_32.fnt"];
	label.position = ccp(_screenW/2, _screenH/16);
	[label runAction:[CCScaleTo actionWithDuration:2.0f scale:1.4f]];
    
    // Update Score Method
    [self updateSpreeScore];
    
    // Remove Label
	[label runAction:[CCSequence actions:
					  [CCFadeOut actionWithDuration:2.0f],
					  [CCCallFuncND actionWithTarget:label selector:@selector(removeFromParentAndCleanup:) data:(void*)YES],
					  nil]];
	[self addChild:label];
}

- (void) showHit {
    // A bad slide, deducting 1 points
    
    // Display bad slide on the bottom of the screen to let users know that they failed
	NSString *str = @"BAD SLIDE";
	CCLabelBMFont *label = [CCLabelBMFont labelWithString:str fntFile:@"good_dog_plain_32.fnt"];
	label.position = ccp(_screenW/2, _screenH/16);
	[label runAction:[CCScaleTo actionWithDuration:1.0f scale:1.2f]];
    
    // Subtract a point
    [self updateBadScore];
    
    // Remove label
	[label runAction:[CCSequence actions:
					  [CCFadeOut actionWithDuration:1.0f],
					  [CCCallFuncND actionWithTarget:label selector:@selector(removeFromParentAndCleanup:) data:(void*)YES],
					  nil]];
	[self addChild:label];
}

- (void) dealloc {
    // Deallocating the memory
	self.sky = nil;
	self.terrain = nil;
	self.hero = nil;
	self.resetButton = nil;
#ifdef DRAW_BOX2D_WORLD
	delete _render;
	_render = NULL;
#endif
	delete _world;
	_world = NULL;
	[super dealloc];
}

@end
