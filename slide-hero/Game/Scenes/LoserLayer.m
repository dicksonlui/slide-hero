

#import "LoserLayer.h"
#import "Game.h"

@implementation LoserLayer

-(id) init
{
    if (self = [super init])
    {
        //add the background image
        CCSprite *backgroundImage = [CCSprite spriteWithFile:@"lose-background.png"];
        backgroundImage.position = ccp(self.contentSize.width/2, self.contentSize.height/2);
        [self addChild:backgroundImage];
        
        //create the play button
        CCMenuItemImage *playButton = [CCMenuItemImage itemFromNormalImage:@"lose-button.png" selectedImage:@"lose-button.png" target: self selector:@selector(playGame)];
        //for now, we will go to level 1
        [playButton setTag:1];
        
        CCMenu *myMenu = [CCMenu menuWithItems:playButton, nil];
        myMenu.position = ccp(myMenu.position.x, 100);
        
        //align the menu images
        [myMenu alignItemsVertically];
        
        [self addChild: myMenu];
    }
    return self;
}

-(void) playGame
{
    NSLog(@"Play button has been pressed.");
    [[CCDirector sharedDirector] replaceScene: (CCScene*)[[Game alloc] init]];
}

+(id) scene
{
    CCScene *scene = [CCScene node];
    
    // 'layer' is an autorelease object.
    Game *layer = [StartMenuLayer node];
    
    // add layer as a child to scene
    [scene addChild: layer];
    
    // return the scene
    return scene;
}

@end