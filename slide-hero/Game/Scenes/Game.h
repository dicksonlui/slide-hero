
#import "cocos2d.h"
#import "Box2D.h"
#import "GLES-Render.h"

@class Sky;
@class Terrain;
@class Hero;

@interface Game : CCLayer {
    int currScore;
	int _screenW;
	int _screenH;
    bool dead;
	b2World *_world;
	Sky *_sky;
	Terrain *_terrain;
	Hero *_hero;
	GLESDebugDraw *_render;
    CCSprite *_deadButton;
	CCSprite *_resetButton;
    CCLabelTTF *_scoreLabel;
}
@property (readonly) int screenW;
@property (readonly) int screenH;
@property (nonatomic, readonly) b2World *world;
@property (nonatomic, retain) Sky *sky;
@property (nonatomic, retain) Terrain *terrain;
@property (nonatomic, retain) Hero *hero;
@property (nonatomic, retain) CCSprite *resetButton;
@property (nonatomic, retain) CCSprite *deadButton;

+ (CCScene*) scene;

- (void) showPerfectSlide;
- (void) showFrenzy;
- (void) showHit;
- (void) updatePerfectScore;
- (void) updateSpreeScore;
- (void) updateBadScore;
- (void) deadPlayer;

@end
